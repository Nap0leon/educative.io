// String
workbox.routing.registerRoute(
    // Matches a Request for the myTargetFile.js file
    "/myTargetFile.js",
    handlerFn
);

// Regular expression
workbox.routing.registerRoute(
    // Matches image files
    /\.(?:png|gif|jpg|jpeg|svg)$/,
    handlerFn
);

// Callback
const myCallBackFn = ({ url, event }) => {
    // Here we can implement our custom matching criteria

    // If we want the route to match: return true
    return true;
};

const handlerFn = async ({ url, event, params }) => {
    return new Response();
    // Do something ...
};

workbox.routing.registerRoute(myCallBackFn, handlerFn);


// Caching strategies (all possible methods)
workbox.routing.registerRoute(
    /\.css$/,
    new workbox.strategies.StaleWhileRevalidate({
        // We can provide a custom name for the cache
        cacheName: "css-cache",
    });

    new workbox.strategies.NetworkFirst({
        // We can provide a custom name for the cache
        cacheName: "css-cache",
    });

    new workbox.strategies.CacheFirst({
        // We can provide a custom name for the cache
        cacheName: "css-cache",
    });

    new workbox.strategies.NetworkOnly({
        // We can provide a custom name for the cache
        cacheName: "css-cache",
    })

    new workbox.strategies.CacheOnly({
        // We can provide a custom name for the cache
        cacheName: "css-cache",
    })
);
