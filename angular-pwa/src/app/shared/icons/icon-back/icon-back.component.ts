import { Component, ViewEncapsulation } from '@angular/core';
import { IconAbstractComponent } from '../icon-abstract/icon-abstract.component';

@Component({
  selector: 'app-icon-back',
  templateUrl: './icon-back.component.html',
  styleUrls: ['../icon-abstract/icon-abstract.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IconBackComponent extends IconAbstractComponent {}
