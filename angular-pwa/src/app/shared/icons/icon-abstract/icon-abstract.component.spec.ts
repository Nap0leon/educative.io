import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconAbstractComponent } from './icon-abstract.component';

describe('IconAbstractComponent', () => {
  let component: IconAbstractComponent;
  let fixture: ComponentFixture<IconAbstractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconAbstractComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconAbstractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
