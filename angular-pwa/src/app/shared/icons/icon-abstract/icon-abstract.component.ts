import {
  Component,
  HostBinding,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'app-icon-abstract',
  templateUrl: './icon-abstract.component.html',
  styleUrls: ['./icon-abstract.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IconAbstractComponent {
  @HostBinding('class.icon-abstract') haveNurunGuideIconClass = true;

  @Input('aria-label') ariaLabel: string | undefined;
}
