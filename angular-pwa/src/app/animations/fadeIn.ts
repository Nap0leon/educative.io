import { animate, style, transition, trigger } from "@angular/animations";

export const enterAnimationFadeIn = [
    trigger('fadeIn', [
        transition(':enter', [style({ opacity: 0 }), animate('1s cubic-bezier(0.075, 0.82, 0.165, 1)', style({ opacity: 1 }))])
    ])
];
