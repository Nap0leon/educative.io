import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CocotteComponent } from './cocotte.component';

describe('CocotteComponent', () => {
  let component: CocotteComponent;
  let fixture: ComponentFixture<CocotteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CocotteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CocotteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
